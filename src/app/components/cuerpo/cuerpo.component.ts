import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cuerpo',
  templateUrl: './cuerpo.component.html',
  styleUrls: ['./cuerpo.component.css']
})
export class CuerpoComponent implements OnInit {
  num=0;
  mensaje:boolean=false;
  constructor() { }

  ngOnInit(): void {
  }

  noMayor():void{
    if (this.num==50) {
      this.num=50;
      this.mensaje=true;
    }else{
      this.num=this.num+1;
      this.mensaje=false;
    }
  }

  noMenor():void{
    if (this.num==0) {
      this.num=0;
      this.mensaje=true;
    } else {
      this.num=this.num-1;
      this.mensaje=false;
    }
  }
}
